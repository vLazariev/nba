import { memo } from "react";
import { Background } from "../../components";

function Marketplace() {
  return (
    <Background>
      <div className="flex-col flex max-w-3xl-1/3 mt-[58px] items-center justify-around">
        <div className="flex-col flex max-w-3xl-1/3 items-center">
          <div className="font-bold text-theme-red-400 text-center text-4xl-38px  w-[500px]">
            Marketplace
          </div>

          <div className="flex flex-col leading-7 text-base-6/8-s font-semibold mt-8 text-center w-[540px]">
            <p>Marketplace</p>
          </div>
        </div>
      </div>
    </Background>
  );
}

export default memo(Marketplace);
