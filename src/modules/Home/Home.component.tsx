import { memo } from "react";
import {
  Background,
  LeftHomeBlock,
  LeftToRightSlider,
  RightToLeftSlider,
  Footer,
} from "../../components";
import { RightHomeBlock } from "../../components/RightHomeBlock";

function Home() {
  return (
    <Background>
      <div className="flex md:flex-row flex-col-reverse justify-between flex-grow z-10">
        <LeftHomeBlock />
        <RightHomeBlock />
      </div>
      <div className="flex flex-col flex-grow w-full">
        <LeftToRightSlider />
        <RightToLeftSlider />
      </div>
      <div className="flex flex-col flex-grow w-full mt-[150px]">
        <Footer />
      </div>
    </Background>
  );
}

export default memo(Home);
