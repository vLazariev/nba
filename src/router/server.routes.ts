enum SERVER_ROUTES {
  GET_USER_CREDENTIALS = "/auth/getUserCredentials/",
  USERS_EVENT = "/events/users/",
  USER_CONTACTS = "/users/",
  EVENT = "/events/",
  GET_STATE = "/get-state",
  GET_GUEST_LOGIN = "/auth/guest-login",
}

export default SERVER_ROUTES;
