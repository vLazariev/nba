import { lazy } from "react";

import IRoute from "../types/IRoute";
import APP_ROUTES from "./app.routes";

const routes: IRoute[] = [
  {
    path: "/",
    exact: true,
    private: false,
    redirect: APP_ROUTES.HOME,
    fallback: <div> Loading... </div>,
  },
  {
    path: APP_ROUTES.HOME,
    exact: true,
    private: false,
    component: lazy(() => import("../modules/Home/Home.component")),
    fallback: <div> Loading... </div>,
  },
  {
    path: APP_ROUTES.MARKETPLACE,
    exact: true,
    private: false,
    component: lazy(
      () => import("../modules/Marketplace/Marketplace.component")
    ),
    fallback: <div> Loading... </div>,
  },
  {
    path: APP_ROUTES.GAMES,
    exact: true,
    private: false,
    component: lazy(() => import("../modules/Games/Games.component")),
    fallback: <div> Loading... </div>,
  },
  {
    path: APP_ROUTES.CHALLENGE,
    exact: true,
    private: false,
    component: lazy(() => import("../modules/Challenge/Challenge.component")),
    fallback: <div> Loading... </div>,
  },
  {
    path: APP_ROUTES.LEADERBOARD,
    exact: true,
    private: false,
    component: lazy(
      () => import("../modules/Leaderboard/Leaderboard.component")
    ),
    fallback: <div> Loading... </div>,
  },
  {
    path: APP_ROUTES.CONNECT_WALLET,
    exact: true,
    private: false,
    component: lazy(
      () => import("../modules/ConnectWallet/ConnectWallet.component")
    ),
    fallback: <div> Loading... </div>,
  },
];

export default routes;
