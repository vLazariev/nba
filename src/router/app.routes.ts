enum APP_ROUTES {
  HOME = "/home",
  MARKETPLACE = "/marketplace",
  GAMES = "/games",
  CHALLENGE = "/challenge",
  LEADERBOARD = "/leaderboard",
  CONNECT_WALLET = "/connectWallet",
}

export default APP_ROUTES;
