import React from "react";

//example https://medium.com/@michael.kutateladze/react-router-with-a-single-config-file-61777f306b4f
export default interface IRoute {
  // Path, like in basic prop
  path: string;
  // Exact, like in basic prop
  exact: boolean;
  // Preloader for lazy loading
  fallback: NonNullable<React.ReactNode> | null;
  // Lazy Loaded component
  component?: React.LazyExoticComponent<React.ComponentType<any>>;
  // Sub routes
  routes?: IRoute[];
  // Redirect path
  redirect?: string;
  // If router is private, this is going to be true
  private?: boolean;
  authenticated?: boolean;
  isRequiredFields?: boolean;
}
