import { memo } from "react";
import IconRightCenterLogo from "../../assets/images/e-icon.png";

import "./RightCenterLogo.styles.scss";

function RightCenterLogo() {
  return (
    <div className="east-conf-container flex relative mt-8 ">
      <img
        className="e-icon"
        src={IconRightCenterLogo}
        alt="white-over-red-mini-logo"
      />
      <p className="eastern-conference">eastern conference</p>
    </div>
  );
}

export default memo(RightCenterLogo);
