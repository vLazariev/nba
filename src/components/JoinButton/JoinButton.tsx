import { memo } from "react";

import "./JoinButton.styles.scss";

function JoinButton() {
  return <button className="Rectangle">JOIN NOW</button>;
}

export default memo(JoinButton);
