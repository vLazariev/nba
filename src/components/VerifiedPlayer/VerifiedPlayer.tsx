import { memo } from "react";
import IconLeftLogo from "../../assets/images/verified-oval-2.png";
import "./VerifiedPlayer.styles.scss";

function VerifiedPlayer() {
  return (
    <div className="verified-rectangle flex">
      <img src={IconLeftLogo} alt="white-over-red-mini-logo" />
      <div className="ml-2.5 mt-3.5">
        <p className="stephen">STEPHEN</p>
        <p className="curry">CURRY</p>
        <p className="verified-players">VERIFIED players</p>
      </div>
    </div>
  );
}

export default memo(VerifiedPlayer);
