import { memo } from "react";
import IconLeftCenterLogo from "../../assets/images/w-icon.png";

import "./LeftCenterLogo.styles.scss";

function LeftCenterLogo() {
  return (
    <div className="flex relative mt-32">
      <img
        className="w-icon"
        src={IconLeftCenterLogo}
        alt="white-over-red-mini-logo"
      />
      <p className="western-conference">western conference</p>
    </div>
  );
}

export default memo(LeftCenterLogo);
