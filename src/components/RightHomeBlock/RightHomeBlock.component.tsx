import { memo } from "react";
import PlayerCards from "../../assets/images/player-cards.png";
import Spray from "../../assets/images/spray.png";
import "./RightHomeBlock.styles.scss";

function RightHomeBlock() {
  return (
    <div className="sub-container">
      <img src={PlayerCards} alt="player-cards"></img>
      <img src={Spray} className="spray" alt="Spray"></img>
    </div>
  );
}

export default memo(RightHomeBlock);
