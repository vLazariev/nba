import { memo } from "react";

import "./LeftToRightSlider.styles.scss";
import Jazz from "../../assets/images/LTR-jazz.png";
import Sacramento from "../../assets/images/LTR-sacramento.png";
import Spurs from "../../assets/images/LTR-spurs.png";
import CourtLeft from "../../assets/images/court-left.png";

function LeftToRightSlider() {
  return (
    <>
      <img
        src={CourtLeft}
        alt="courtLeft"
        className="absolute left-[-150px] z-[1]"
      />
      <div className="left-to-right ltr-wrapper z-10">
        <div className="ltr-wrapper__first-half">
          <img src={Jazz} alt="jazz" />
          <img src={Sacramento} alt="Sacramento" />
          <img src={Spurs} alt="Spurs" />
          <img src={Jazz} alt="jazz" />
          <img src={Sacramento} alt="Sacramento" />
          <img src={Spurs} alt="Spurs" />
          <img src={Jazz} alt="jazz" />
          <img src={Sacramento} alt="Sacramento" />
          <img src={Spurs} alt="Spurs" />
          <img src={Jazz} alt="jazz" />
          <img src={Sacramento} alt="Sacramento" />
          <img src={Spurs} alt="Spurs" />
        </div>
        <div className="ltr-wrapper__second-half">
          <img src={Jazz} alt="jazz" />
          <img src={Sacramento} alt="Sacramento" />
          <img src={Spurs} alt="Spurs" />
          <img src={Jazz} alt="jazz" />
          <img src={Sacramento} alt="Sacramento" />
          <img src={Spurs} alt="Spurs" />
          <img src={Jazz} alt="jazz" />
          <img src={Sacramento} alt="Sacramento" />
          <img src={Spurs} alt="Spurs" />
          <img src={Jazz} alt="jazz" />
          <img src={Sacramento} alt="Sacramento" />
          <img src={Spurs} alt="Spurs" />
        </div>
      </div>
    </>
  );
}

export default memo(LeftToRightSlider);
