import { memo } from "react";

import "./RightToLeftSlider.styles.scss";
import Celtics from "../../assets/images/RTL-celtics.png";
import Hawks from "../../assets/images/RTL-hawks.png";
import Nets from "../../assets/images/RTL-nets.png";
import CourtRight from "../../assets/images/court-right.png";
import { RightCenterLogo } from "../RightCenterLogo";

function RightToLeftSlider() {
  return (
    <>
      <img
        src={CourtRight}
        alt="courtLeft"
        className="absolute md+1:right-[-150px] right-0 bottom-[-550px] z-[1]"
      />
      <div className="right-to-left rtl-wrapper z-10">
        <div className="rtl-wrapper__first-half">
          <img src={Celtics} alt="jazz" />
          <img src={Hawks} alt="Sacramento" />
          <img src={Nets} alt="Spurs" />
          <img src={Celtics} alt="jazz" />
          <img src={Hawks} alt="Sacramento" />
          <img src={Nets} alt="Spurs" />
          <img src={Celtics} alt="jazz" />
          <img src={Hawks} alt="Sacramento" />
          <img src={Nets} alt="Spurs" />
          <img src={Celtics} alt="jazz" />
          <img src={Hawks} alt="Sacramento" />
          <img src={Nets} alt="Spurs" />
        </div>
        <div className="rtl-wrapper__second-half">
          <img src={Celtics} alt="jazz" />
          <img src={Hawks} alt="Sacramento" />
          <img src={Nets} alt="Spurs" />
          <img src={Celtics} alt="jazz" />
          <img src={Hawks} alt="Sacramento" />
          <img src={Nets} alt="Spurs" />
          <img src={Celtics} alt="jazz" />
          <img src={Hawks} alt="Sacramento" />
          <img src={Nets} alt="Spurs" />
          <img src={Celtics} alt="jazz" />
          <img src={Hawks} alt="Sacramento" />
          <img src={Nets} alt="Spurs" />
        </div>
      </div>
      <RightCenterLogo />
    </>
  );
}

export default memo(RightToLeftSlider);
