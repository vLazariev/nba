import { memo } from "react";
import {
  JoinButton,
  LeftCenterLogo,
  RightCenterLogo,
  VerifiedPlayer,
} from "..";
import "./LeftHomeBlock.styles.scss";

function LeftHomeBlock() {
  return (
    <div>
      <div className="Own-The-Best-Rule-T">
        <p>OWN THE </p>
        <p style={{ color: "#ffcc3e" }}>BEST.</p>
        <p>RULE THE </p>
        <p style={{ color: "#ffcc3e" }}>GAME.</p>
      </div>
      <div className="Buy-sell-and-colle">
        <p>
          Buy, sell, and collect official NBA players to play and earn.
          Superhuman is a virtual gaming universe with upgradable characters,
          high rewarding games and exclusive offers for sports fans.
        </p>
      </div>

      <JoinButton />
      <VerifiedPlayer />
      <LeftCenterLogo />
    </div>
  );
}

export default memo(LeftHomeBlock);
