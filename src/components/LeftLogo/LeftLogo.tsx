import { memo } from "react";
import IconLeftLogo from "../../assets/images/superhuman-logo.png";

function LeftLogo() {
  return (
    <div className="flex w-[139px] h-[36px] object-contain ml-[80px] ">
      <img src={IconLeftLogo} alt="white-over-red-mini-logo" />
    </div>
  );
}

export default memo(LeftLogo);
