import { Suspense } from "react";
import { Redirect, Route } from "react-router-dom";
import IRoute from "../../types/IRoute";

export function RouteWithSubRoutes({
  authenticated,
  fallback,
  path,
  redirect,
  isRequiredFields,
  ...route
}: IRoute) {
  return (
    <Suspense fallback={fallback}>
      <Route
        path={path}
        render={(props: any) => {
          if (redirect) {
            return <Redirect to={redirect} />;
          }

          return (
            route.component && (
              <route.component {...props} routes={route.routes} />
            )
          );
        }}
      />
    </Suspense>
  );
}

export default RouteWithSubRoutes;
