import { Switch } from "react-router";
import { memo } from "react";

import IRoute from "../../types/IRoute";
import RouteWithSubRoutes from "./RouteWithSubRoutes.component";

interface IProps {
  routes: IRoute[];
}

export function Router({ routes }: IProps) {
  return (
    <Switch>
      {routes.map((route: IRoute) => (
        <RouteWithSubRoutes key={route.path} {...route} />
      ))}
    </Switch>
  );
}
export default memo(Router);
