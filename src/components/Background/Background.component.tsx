import React, { memo } from "react";
import { HeaderMenu, LeftLogo } from "..";
import BackgroundDots from "../../assets/images/bg-left-0.png";
import classnames from "classnames";

import "./Background.styles.scss";

interface IProps {
  className?: string;
  children: React.ReactNode;
  classNames?: { childrenWrp?: string };
}

function Background({ className, classNames, children, ...rest }: IProps) {
  const classes = classnames(className, "bg w-full h-full");

  return (
    <div className={classes} {...rest}>
      {/* <img src={BackgroundDots} alt="bg-dots" className="bg-0"></img> */}

      <div className="bg__content h-full bg-no-repeat bg-auto flex flex-col">
        <HeaderMenu />
        <div className="flex justify-between flex-col flex-grow mt-8">
          {children}
        </div>
      </div>
    </div>
  );
}
export default memo(Background);
