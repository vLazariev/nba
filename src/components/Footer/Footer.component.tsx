import React, { memo } from "react";
import classnames from "classnames";

import "./Footer.styles.scss";

interface IProps {
  className?: string;
  classNames?: { childrenWrp?: string };
}

function Footer({ className, classNames, ...rest }: IProps) {
  const classes = classnames(className, "flex ");

  return (
    <div className={classes} {...rest}>
      <div className="h-[720px] md+1:h-[390px] footer-rectangle flex md+1:flex-row flex-col sm:justify-between items-center">
        <div>
          <p className="head-text">lorem IPSUM</p>
          <p className="middle-text">consectetur adipiscing elit</p>
          <p className="bottom-text">
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
            accusantium doloremque laudantium, totam remomnis iste natus error.
          </p>
        </div>
        <div>
          <p className="head-text">lorem IPSUM</p>
          <p className="middle-text">consectetur adipiscing elit</p>
          <p className="bottom-text">
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
            accusantium doloremque laudantium, totam remomnis iste natus error.
          </p>
        </div>
        <div>
          <p className="head-text">lorem IPSUM</p>
          <p className="middle-text">consectetur adipiscing elit</p>
          <p className="bottom-text">
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
            accusantium doloremque laudantium, totam remomnis iste natus error.
          </p>
        </div>
      </div>
    </div>
  );
}
export default memo(Footer);
