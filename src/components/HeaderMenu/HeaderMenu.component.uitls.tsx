export function changeResponsiveHeader() {
  let x = document.getElementById("myTopnav");
  if (x) {
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }
}
export function detectLocation() {
  if (window.location.pathname.match("/home")) {
    return "Home";
  } else if (window.location.pathname.match("/marketplace")) {
    return "Marketplace";
  } else if (window.location.pathname.match("/games")) {
    return "Games";
  } else if (window.location.pathname.match("/challenge")) {
    return "Challenge";
  } else if (window.location.pathname.match("/leaderboard")) {
    return "Leaderboard";
  } else if (window.location.pathname.match("/connectWallet")) {
    return "ConnectWallet";
  } else {
    return "Home";
  }
}
