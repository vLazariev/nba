import React, { memo } from "react";
import classnames from "classnames";

import "./HeaderMenu.styles.scss";
import { APP_ROUTES } from "../../router";
import { LeftLogo } from "..";
import { Link } from "react-router-dom";
import {
  detectLocation,
  changeResponsiveHeader,
} from "./HeaderMenu.component.uitls";

interface IProps {
  className?: string;
  classNames?: { childrenWrp?: string };
}

function HeaderMenu({ className, classNames, ...rest }: IProps) {
  const classes = classnames(className, "text-right");

  console.log(window.location);
  const currentLocation = detectLocation() as String;
  return (
    <div className={classes} {...rest}>
      <div className="bg-announcement ">
        <div className="scrolling-announcement ticker-wrapper">
          <div className="ticker-wrapper__first-half">
            <p>Lorem ipsum dolor sit amet</p>
            <p>Excepteur sint occaecat cupidatat non proident</p>
          </div>
          <div className="ticker-wrapper__second-half">
            <p>Lorem ipsum dolor sit amet</p>
            <p>Excepteur sint occaecat cupidatat non proident</p>
          </div>
        </div>
      </div>

      <div className="flex laptop:flex-row flex-col justify-between mt-8">
        <div className="">
          <LeftLogo />
        </div>
        <div className="topnav" id="myTopnav">
          <Link
            to={APP_ROUTES.HOME}
            className={currentLocation === "Home" ? "active" : ""}
          >
            HOME
          </Link>
          <Link
            to={APP_ROUTES.MARKETPLACE}
            className={currentLocation === "Marketplace" ? "active" : ""}
          >
            MARKETPLACE
          </Link>
          <Link
            to={APP_ROUTES.GAMES}
            className={currentLocation === "Games" ? "active" : "disabled"}
          >
            GAMES <p>soon</p>
          </Link>
          <Link
            to={APP_ROUTES.CHALLENGE}
            className={currentLocation === "Challenge" ? "active" : "disabled"}
          >
            CHALLENGE <p>soon</p>
          </Link>
          <Link
            to={APP_ROUTES.LEADERBOARD}
            className={
              currentLocation === "Leaderboard" ? "active" : "disabled"
            }
          >
            LEADERBOARD <p>soon</p>
          </Link>
          <Link
            to={APP_ROUTES.CONNECT_WALLET}
            className={currentLocation === "ConnectWallet" ? "active" : ""}
          >
            CONNECT WALLET
          </Link>
          <button className="icon" onClick={changeResponsiveHeader}>
            <i className="fa fa-bars"></i>
          </button>
        </div>
      </div>
    </div>
  );
}
export default memo(HeaderMenu);
