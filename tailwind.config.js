module.exports = {
  mode: "jit",
  purge: [
    "./src/**/*.html",
    "./src/**/*.tsx",
    "./src/**/*.ts",
    "./src/safelist.txt",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    screens: {
      tablet: "640px",
      laptop: "1024px",
      desktop: "1280px",
      sm: "640px",
      "md+1": "769px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
      "2xl": "1536px",
    },
  },
  variants: {
    extend: {
      fontFamily: {
        IBM: ["IBMPlexSans"],
        Chaney: [
          "Chaney Ultra Extended",
          "Chaney Extended",
          "Chaney",
          "Chaney Wide",
        ],
      },
    },
  },
  plugins: [],
};
